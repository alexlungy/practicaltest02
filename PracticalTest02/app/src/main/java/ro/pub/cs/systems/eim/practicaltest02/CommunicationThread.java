package ro.pub.cs.systems.eim.practicaltest02;

import android.os.AsyncTask;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;

public class CommunicationThread extends Thread {

    private Socket socket;
    private EditText clientEditText;

    public CommunicationThread(Socket socket, EditText clientEditText) {
        this.socket = socket;
        this.clientEditText = clientEditText;
    }

    @Override
    public void run() {
        try {
            Log.v(Constants.TAG, "Connection opened with " + socket.getInetAddress() + ":" + socket.getLocalPort());

            PrintWriter pw = Utilities.getWriter(socket);
            BufferedReader reader = Utilities.getReader(socket);



            WebServiceTask service = new WebServiceTask(pw);

            String line = reader.readLine();
            service.doInBackground(line);

            Log.e(Constants.TAG, "EXECUTE WITH " + line);

            while (service.getStatus() == AsyncTask.Status.RUNNING)
            {
            }

            pw.print(WebServiceTask.result);

            pw.flush();
            Log.e(Constants.TAG, "DONE");


            socket.close();
            Log.v(Constants.TAG, "Connection closed");
        } catch (Exception exception) {
            Log.e(Constants.TAG, "An exception has occurred: " + exception.getMessage());
        }
    }

}
