package ro.pub.cs.systems.eim.practicaltest02;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.client.HttpClient;
import cz.msebera.android.httpclient.client.ResponseHandler;
import cz.msebera.android.httpclient.client.methods.HttpGet;
import cz.msebera.android.httpclient.impl.client.BasicResponseHandler;
import cz.msebera.android.httpclient.impl.client.DefaultHttpClient;

import static java.net.Proxy.Type.HTTP;

public class WebServiceTask extends AsyncTask<String, String, String> {

    private PrintWriter printWriter;

    public WebServiceTask(PrintWriter printWriter) {
        this.printWriter = printWriter;
    }

    public static String result;

    @Override
    protected String doInBackground(String... params) {
        String queryText = params[0];



        if (queryText == "" )
        {
            return null;
        }

        // create an instance of a HttpClient object
        HttpClient client = new DefaultHttpClient();

        // get method used for sending request from methodsSpinner

        HttpGet get = new HttpGet(Constants.GET_WEB_SERVICE_ADDRESS
                + queryText);


        ResponseHandler<String> handler = new BasicResponseHandler();
        try {
            String content = client.execute(get, handler);
            String finalResult = "";

            JSONObject jsonObject = new JSONObject(content);

            JSONArray jsonArray = jsonObject.getJSONArray("RESULTS");

            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                finalResult += object.getString("name") + ",\n";
            }

            result = finalResult;
            return finalResult;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return null;
    }


    @Override
    protected void onProgressUpdate(String... progress) {
        // send the result to writer
        printWriter.println(progress[0]);
        Log.e(Constants.TAG, progress[0]);
        printWriter.flush();
    }

}
