package ro.pub.cs.systems.eim.practicaltest02;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class PracticalTest02MainActivity extends AppCompatActivity {


    Button startServer;
    Button sendQuery;

    EditText serverPort;
    EditText clientPort;
    EditText queryText;

    TextView result;

    ServerThread serverThread;


    private StartServerButtonClickListener startServerButtonClickListener = new StartServerButtonClickListener();
    private class StartServerButtonClickListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {
            String port = serverPort.getText().toString();
            serverThread = new ServerThread(port, result, queryText);
            serverThread.startServer();
        }
    }



    private SendQueryListener sendQueryListener = new SendQueryListener();
    private class SendQueryListener implements View.OnClickListener {

        @Override
        public void onClick(View view) {

            ClientAsyncTask thr = new ClientAsyncTask(queryText, Integer.parseInt(clientPort.getText().toString()), result);
            thr.execute();

//            WebServiceTask webServiceTask = new WebServiceTask(result);
   //         webServiceTask.execute(queryText.getText().toString());
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_practical_test02_main);

        startServer = findViewById(R.id.start_server);
        sendQuery = findViewById(R.id.send_query);

        serverPort = findViewById(R.id.port_server);
        clientPort = findViewById(R.id.port_client);
        queryText = findViewById(R.id.query);

        result = findViewById(R.id.cl_result);

        startServer.setOnClickListener(startServerButtonClickListener);
        sendQuery.setOnClickListener(sendQueryListener);
    }
}
