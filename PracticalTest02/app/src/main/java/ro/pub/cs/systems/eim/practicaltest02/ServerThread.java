package ro.pub.cs.systems.eim.practicaltest02;

import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerThread extends Thread {

    private final TextView resultText;
    private final EditText clientText;
    private boolean isRunning;

    private ServerSocket serverSocket;

    private int port;

    public ServerThread(String port, TextView resultText, EditText clientText) {
        this.port = Integer.parseInt(port);
        this.resultText = resultText;
        this.clientText = clientText;
        //startServer();
    }

    public void startServer() {
        isRunning = true;
        start();
        Log.v(Constants.TAG, "startServer() method was invoked");
    }

    public void stopServer() {
        isRunning = false;
        try {
            serverSocket.close();
        } catch (IOException ioException) {
            Log.e(Constants.TAG, "An exception has occurred: " + ioException.getMessage());
        }
        Log.v(Constants.TAG, "stopServer() method was invoked");
    }

    @Override
    public void run() {
        try {

            serverSocket = new ServerSocket(port);
            while (isRunning) {
                Socket socket = serverSocket.accept();
                if (socket != null) {

                    CommunicationThread communicationThread = new CommunicationThread(socket, clientText);
                    communicationThread.start();


                }
            }

        } catch (Exception ioException) {
            Log.e(Constants.TAG, "An exception has occurred: " + ioException.getMessage());
        }
    }
}
