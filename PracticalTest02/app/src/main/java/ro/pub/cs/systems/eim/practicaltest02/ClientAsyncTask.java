package ro.pub.cs.systems.eim.practicaltest02;

import android.os.AsyncTask;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;

public class ClientAsyncTask extends AsyncTask<String, String, Void> {

    private final TextView result;
    EditText queryText;
    int port;

    public ClientAsyncTask(EditText q, int p, TextView r){
        queryText = q;
        port = p;
        result = r;
    }

    @Override
    public Void doInBackground(String... params) {
        try {
            Socket socket = new Socket("localhost", port);

            PrintWriter writer = Utilities.getWriter(socket);
            BufferedReader reader = Utilities.getReader(socket);
            writer.println(queryText.getText().toString());

            Log.e(Constants.TAG, "WAIAI");
            String line = "";
            while ((line = reader.readLine()) != null){
                Log.e(Constants.TAG, line);
                publishProgress(line);
            }
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        Log.e(Constants.TAG, values[0]);
        result.append(values[0]);
    }
}
